## cras_ros_utils (noetic) - 2.5.1-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Tue, 18 Feb 2025 16:08:48 -0000`

These packages were released:
- `cras_bag_tools`
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.5.0-1`
- old version: `2.5.0-1`
- new version: `2.5.1-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.5.0-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 13 Feb 2025 01:37:56 -0000`

These packages were released:
- `cras_bag_tools`
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.8-1`
- old version: `2.4.8-1`
- new version: `2.5.0-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.8-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Tue, 07 Jan 2025 11:02:14 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.7-1`
- old version: `2.4.7-1`
- new version: `2.4.8-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.7-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 12 Dec 2024 13:31:20 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.5-1`
- old version: `2.4.6-1`
- new version: `2.4.7-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.6-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 12 Dec 2024 11:33:35 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.5-1`
- old version: `2.4.5-1`
- new version: `2.4.6-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.5-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Sat, 02 Nov 2024 15:41:09 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.4-1`
- old version: `2.4.4-1`
- new version: `2.4.5-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `1.0.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.4-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Sat, 14 Sep 2024 01:45:22 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.4.2-1`
- old version: `2.4.2-1`
- new version: `2.4.4-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `0.9.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.2-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release cras_ros_utils -r noetic` on `Thu, 05 Sep 2024 11:35:34 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.9-1`
- old version: `2.4.1-1`
- new version: `2.4.2-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `0.9.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.4.1-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release cras_ros_utils -r noetic` on `Wed, 04 Sep 2024 18:55:24 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.9-1`
- old version: `2.3.9-1`
- new version: `2.4.1-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.25.1`
- rosdistro version: `0.9.1`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.9-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Tue, 27 Feb 2024 17:33:01 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.8-1`
- old version: `2.3.8-1`
- new version: `2.3.9-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.8-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 12 Jan 2024 19:09:47 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.7-1`
- old version: `2.3.7-1`
- new version: `2.3.8-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.7-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Tue, 09 Jan 2024 18:03:28 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.5-1`
- old version: `2.3.5-1`
- new version: `2.3.7-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.5-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Tue, 21 Nov 2023 16:25:52 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.4-1`
- old version: `2.3.4-1`
- new version: `2.3.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.4-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Wed, 25 Oct 2023 07:55:27 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.3-1`
- old version: `2.3.3-2`
- new version: `2.3.4-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.3-2

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Wed, 25 Oct 2023 07:53:41 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.3-1`
- old version: `2.3.3-1`
- new version: `2.3.3-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.3-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 06 Oct 2023 18:06:50 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.2-1`
- old version: `2.3.2-1`
- new version: `2.3.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.2-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 06 Oct 2023 17:19:09 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.1-1`
- old version: `2.3.1-1`
- new version: `2.3.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.1-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 14 Jul 2023 07:34:28 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.0-2`
- old version: `2.3.0-2`
- new version: `2.3.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.0-2

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 13 Jul 2023 12:02:18 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.3.0-1`
- old version: `2.3.0-1`
- new version: `2.3.0-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.3.0-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Wed, 12 Jul 2023 09:49:44 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.3-1`
- old version: `2.2.3-1`
- new version: `2.3.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.2.3-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 16 Jun 2023 12:39:40 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-1`
- new version: `2.2.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.2.3-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Fri, 16 Jun 2023 12:24:58 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-2`
- new version: `2.2.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.2.2-2

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Fri, 16 Jun 2023 12:21:58 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-1`
- new version: `2.2.2-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.2.2-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Mon, 15 May 2023 09:02:11 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.0-1`
- old version: `2.2.1-1`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.2.2-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Mon, 15 May 2023 08:57:28 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.0-1`
- old version: `2.2.1-1`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.2.1-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Mon, 15 May 2023 03:11:34 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.0-1`
- old version: `2.2.0-1`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.2.1-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Mon, 15 May 2023 03:07:06 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.2.0-1`
- old version: `2.2.0-1`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.2.0-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic cras_ros_utils` on `Sat, 08 Apr 2023 23:26:29 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.1.0-1`
- old version: `2.1.0-1`
- new version: `2.2.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.2.0-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic cras_ros_utils` on `Sat, 08 Apr 2023 23:23:30 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.1.2-1`
- old version: `2.1.2-1`
- new version: `2.2.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.1.2-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 10 Feb 2023 13:43:51 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.1.1-2`
- old version: `2.1.1-2`
- new version: `2.1.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.1.1-2

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Wed, 08 Feb 2023 23:14:02 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.10-2`
- old version: `2.1.1-1`
- new version: `2.1.1-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.1.0-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Wed, 08 Feb 2023 22:13:08 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_docs_common`
- `cras_py_common`
- `cras_topic_tools`
- `image_transport_codecs`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.10-1`
- old version: `2.0.10-1`
- new version: `2.1.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.10-2

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 25 Nov 2022 08:35:41 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.10-1`
- new version: `2.0.10-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.10-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 24 Nov 2022 16:47:20 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.9-1`
- new version: `2.0.10-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.10-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Thu, 24 Nov 2022 16:44:49 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.9-1`
- new version: `2.0.10-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.9-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 24 Nov 2022 16:36:40 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.8-1`
- new version: `2.0.9-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.9-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Thu, 24 Nov 2022 16:34:47 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.8-1`
- new version: `2.0.9-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.8-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 24 Nov 2022 15:04:29 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.7-1`
- new version: `2.0.8-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.8-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Thu, 24 Nov 2022 15:01:56 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.7-1`
- new version: `2.0.8-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.7-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 24 Nov 2022 14:43:24 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.6-1`
- new version: `2.0.7-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.7-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Thu, 24 Nov 2022 14:39:59 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.6-1`
- new version: `2.0.7-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.6-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Thu, 24 Nov 2022 14:08:42 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.5-1`
- new version: `2.0.6-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.6-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Thu, 24 Nov 2022 14:06:27 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.5-1`
- old version: `2.0.5-1`
- new version: `2.0.6-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.5-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Sat, 22 Oct 2022 23:46:23 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.4-1`
- old version: `2.0.4-1`
- new version: `2.0.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.5-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Sat, 22 Oct 2022 23:40:39 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.3-1`
- old version: `2.0.3-1`
- new version: `2.0.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.4-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 14 Oct 2022 08:57:29 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.3-1`
- old version: `2.0.3-1`
- new version: `2.0.4-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.3-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Fri, 07 Oct 2022 07:55:05 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.2-1`
- old version: `2.0.2-1`
- new version: `2.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.3-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils` on `Fri, 07 Oct 2022 07:51:37 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.2-1`
- old version: `2.0.2-1`
- new version: `2.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (noetic) - 2.0.2-1

The packages in the `cras_ros_utils` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_ros_utils --new-track` on `Mon, 29 Aug 2022 20:19:57 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_ros_utils (melodic) - 2.0.2-1

The packages in the `cras_ros_utils` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_ros_utils` on `Mon, 29 Aug 2022 20:15:38 -0000`

These packages were released:
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`

These packages were explicitly ignored:
- `camera_throttle`
- `cras_bag_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_ros_utils`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/ros-utils.git
- rosdistro version: `2.0.1-1`
- old version: `2.0.1-1`
- new version: `2.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_cpp_common (melodic) - 2.0.1-1

The packages in the `cras_cpp_common` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic cras_cpp_common --new-track` on `Fri, 26 Aug 2022 22:11:12 -0000`

These packages were released:
- `camera_throttle`
- `cras_bag_tools`
- `cras_cpp_common`
- `cras_py_common`
- `cras_topic_tools`
- `tf_static_publisher`

Version of package(s) in repository `cras_cpp_common`:

- upstream repository: https://github.com/ctu-vras/ros-utils
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.1`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


